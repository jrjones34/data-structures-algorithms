const selectionSort = (arr) => {
  if (!Array.isArray(arr)) return null;
  if (arr.length <= 1) return arr;

  let lowerBound = 0;

  while (lowerBound < arr.length - 1) {
    const least = getIndexOfLeastValue(arr, lowerBound);
    if (lowerBound !== least) {
      arr = swapLowerBoundAndLeastValues(arr, lowerBound, least);
    }
    lowerBound++;
  }

  return arr;
}

const getIndexOfLeastValue = (arr, lowerBound) => {
  let indexOfLeast = lowerBound;

  for (let i = lowerBound; i < arr.length; i++) {
    indexOfLeast = arr[i] < arr[indexOfLeast] ? i : indexOfLeast;
  }

  return indexOfLeast;
}

const swapLowerBoundAndLeastValues = (arr, lowerBound, least) => {
  const prevLow = arr[lowerBound];

  arr[lowerBound] = arr[least];
  arr[least] = prevLow;

  return arr;
}

export default selectionSort;