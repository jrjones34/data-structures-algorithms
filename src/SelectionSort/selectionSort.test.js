import selectionSort from './selectionSort';


test('Passing a string instead of an array, should return null.', () => {;
  expect(selectionSort("I'm not an array.")).toBe(null);
});

test('Passing an empty array or a single element array should just return the input.', () => {
  // Empty array
  expect(selectionSort([])).toEqual([]);
  
  // Single-element array
  expect(selectionSort([1])).toEqual([1]);
});

test('Passing [2, 1] should return [1, 2].', () => {
  expect(selectionSort([2, 1])).toEqual([1, 2]);
});

test('Passing [4, 1, 5, 2, 3] should return [1, 2, 3, 4, 5]', () => {
  expect(selectionSort([4, 1, 5, 2, 3])).toEqual([1, 2, 3, 4, 5]);
});

test('Passing [4, 2, 7, 1, 3] should return [1, 2, 3, 4, 7].', () => {
  expect(selectionSort([4, 2, 7, 1, 3])).toEqual([1, 2, 3, 4, 7]);
});

test('Passing [9, 0, 8, 7, 3, 5, 1, 2, 6, 4] should return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].', () => {
  expect(selectionSort([9, 0, 8, 7, 3, 5, 1, 2, 6, 4])).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
});

test("Passing ['b', 'a'] should return ['a', 'b'].", () => {
  expect(selectionSort(['b', 'a'])).toEqual(['a', 'b']);
});

test("Passing ['cba', 'cab', 'abc', 'acb', 'bac', 'bca'] should return ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'].", () => {
  expect(selectionSort(['cba', 'cab', 'abc', 'acb', 'bac', 'bca'])).toEqual(['abc', 'acb', 'bac', 'bca', 'cab', 'cba']);
});