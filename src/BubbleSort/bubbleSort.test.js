import bubbleSort from './bubbleSort';


test('Passing a string instead of an array, should return null.', () => {
  expect(bubbleSort("I'm not an array.")).toBe(null);
});

test('Passing an empty array or a single element array should just return the input.', () => {
  // Empty array.
  expect(bubbleSort([])).toEqual([]);

  // Single-element array
  expect(bubbleSort([1])).toEqual([1]);
});

test('Passing [2, 1] should return [1, 2].', () => {
  expect(bubbleSort([2, 1])).toEqual([1, 2]);
});

test('Passing [4, 1, 5, 2, 3] should return [1, 2, 3, 4, 5]', () => {
  expect(bubbleSort([4, 1, 5, 2, 3])).toEqual([1, 2, 3, 4, 5]);
});

test('Passing [4, 2, 7, 1, 3] should return [1, 2, 3, 4, 7].', () => {
  expect(bubbleSort([4, 2, 7, 1, 3])).toEqual([1, 2, 3, 4, 7]);
});

test('Passing [9, 0, 8, 7, 3, 5, 1, 2, 6, 4] should return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].', () => {
  expect(bubbleSort([9, 0, 8, 7, 3, 5, 1, 2, 6, 4])).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
});

test("Passing ['b', 'a'] should return ['a', 'b'].", () => {
  expect(bubbleSort(['b', 'a'])).toEqual(['a', 'b']);
});

test("Passing ['cba', 'cab', 'abc', 'acb', 'bac', 'bca'] should return ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'].", () => {
  expect(bubbleSort(['cba', 'cab', 'abc', 'acb', 'bac', 'bca'])).toEqual(['abc', 'acb', 'bac', 'bca', 'cab', 'cba']);
});