const bubbleSort = (arr) => {
  if (!Array.isArray(arr)) return null;
  if (arr.length <= 1) return arr;

  let upperBound = arr.length - 1;
  let sorted = false;

  while (!sorted) {
    sorted = true;

    for (let i = 0; i < upperBound; i++) {
      let left = arr[i];
      let right = arr[i + 1];

      if (left > right) {
        arr[i] = right;
        arr[i + 1] = left;
        sorted = false;
      }
    }

    upperBound--;
  }

  return arr;
}

export default bubbleSort;