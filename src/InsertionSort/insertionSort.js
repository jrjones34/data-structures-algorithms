const insertionSort = (arr) => {
  if (!Array.isArray(arr)) return null;
  if (arr.length <= 1) return arr;

  for (let i = 1; i < arr.length; i++) {
    let position = i;
    let tempVal = arr[i];

    while (position > 0 && arr[position - 1] > tempVal) {
      arr[position] = arr[position - 1];
      position--;
    }
    arr[position] = tempVal;
  }
  return arr;
}

export default insertionSort;